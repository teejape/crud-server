const config = require('./config/db.config.json');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const routes = require('./routes');
const dbConnection = require('./dbConnection');
const passport  = require('./config/passport.config');
const session = require('express-session');

const root = "./";
const port = 3000;

const app = express();

app.use(bodyParser.json({type: "*/json"}));

app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.join(root, 'dist')));

app.use("/", routes);

app.use(session({ secret: 'passport-tutorial', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false }));

app.get('*', (req, res) => {
    res.status(200).sendFile('dist/index.html', {root});
});

app.listen(port, () => {console.log('server live')});