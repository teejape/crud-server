const express = require("express");
const router = express.Router();
const tasksService = require('./services/tasksService');
const authService = require('./services/authService');
const root = "./";

//@TODO split routes to files

router.get('/tasks', (req, res, next) => {
    res.sendFile('dist/index.html', {root});
});


router.get('/api/tasks', (req, res, next) => {
    tasksService.getTasks(req, res);
});

router.post('/signup', (req, res, next) => {
    authService.createOne(req.body, res);
});

// router.login()

// router.post('/api/add-task', (req, res) => {
//     tasksService.addTask(req, res);
// });
//
// router.post('/api/edit-task', (req, res) => {
//     tasksService.editTask(req, res);
// });

module.exports = router;