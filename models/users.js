const Sequelize = require('sequelize');
const sequelize = require('../dbConnection');

const User = sequelize.define('user', {
    id: {
        type: Sequelize.DataTypes.INTEGER,
        field: 'id',
        primaryKey: true
    },
    name: {
        type: Sequelize.DataTypes.STRING,
        field: 'name'
    },
    password: {
        type: Sequelize.DataTypes.STRING,
        field: 'password'
    }
}, {timestamps: false});

module.exports = User;