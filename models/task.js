const Sequelize = require('sequelize');
const sequelize = require('../dbConnection');

const Task = sequelize.define('task', {
    id: {
       type: Sequelize.DataTypes.INTEGER,
        field: 'id',
        primaryKey: true
    },
    name: {
       type: Sequelize.DataTypes.STRING,
        field: 'name'
    },
    description: {
        type: Sequelize.DataTypes.STRING,
        field: 'description'
    }
}, {timestamps: false});

module.exports = Task;